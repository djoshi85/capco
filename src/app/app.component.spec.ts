import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SubmitRowService } from './submit-row.service';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import { MatTableModule, MatPaginatorModule, MatSortModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let de: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MatTableModule, MatPaginatorModule, MatSortModule, HttpClientTestingModule, BrowserAnimationsModule],
      declarations: [
        AppComponent
      ],
      providers: [  SubmitRowService ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    de = fixture.debugElement;
  });

  it('should create the app', () => {
    const fixture1 = TestBed.createComponent(AppComponent);
    const app = fixture1.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should test the table Header'`, () =>  {
     fixture.detectChanges();
      // has to fallback to query DOM elements and since it has 5 rows by defualt
     const rowHtmlElements = de.nativeElement.querySelectorAll('.rows');
     expect(rowHtmlElements.length).toBe(1);
     // header test
     // tslint:disable-next-line: max-line-length
     expect(de.nativeElement.querySelector('.rows').textContent).toContain('name  phone  email  company  date_entry org_num  address_1  city  zip  geo  pan  pin  id  status  fee  guid  date_exit  date_first  date_recent  url  Submit');
   });
});
