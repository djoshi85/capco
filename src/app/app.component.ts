import { HttpClient } from '@angular/common/http';
import { Component, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { SubmitRowService } from './submit-row.service';
import data from '../assets/sample-data.json';

@Component({
  selector: 'app-root',
  styleUrls: ['./app.component.css'],
  templateUrl: './app.component.html',
})

export class AppComponent {
  displayedColumns: string[] = ['name', 'phone', 'email', 'company', 'date_entry', 'org_num', 'address_1',
'city', 'zip', 'geo', 'pan', 'pin', 'id', 'status', 'fee', 'guid', 'date_exit', 'date_first', 'date_recent', 'url', 'submit'
];
   ELEMENT_DATA: any = [];
   dataSource: any;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private submitRowService: SubmitRowService) { }

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit() {

     this.ELEMENT_DATA = data;
     this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
     this.dataSource.paginator = this.paginator;
  }

  selectRow(rowId, rowStatus) {
    console.log(rowId, rowStatus);
    const passData = { Id: rowId, status: rowStatus };
    this.submitRowService.updateTask(passData);
}}
