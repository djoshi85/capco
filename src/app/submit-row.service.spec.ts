import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SubmitRowService } from './submit-row.service';

describe('SubmitRowService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule]

  }));

  it('should be created', () => {
    const service: SubmitRowService = TestBed.get(SubmitRowService);
    expect(service).toBeTruthy();
  });
});
