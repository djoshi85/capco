import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
type NewType = string;

@Injectable({
  providedIn: 'root'
})
export class SubmitRowService {
  apiUrl: NewType = '/api';
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }

  updateTask(data): Observable<any> {
    const API_URL = `${this.apiUrl}/submit`;
    console.log('service called with data', data);
    return this.http.post(API_URL, data, { headers: this.headers }).pipe(
      catchError(this.error));
  }

  // Handle Errors
  error(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
